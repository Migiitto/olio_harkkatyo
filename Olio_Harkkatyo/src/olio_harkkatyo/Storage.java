
package olio_harkkatyo;

import java.util.ArrayList;

/** Mikael Sommarberg - 0420191
 */
public class Storage {
    private ArrayList<Package> packages = new ArrayList();
    private static Storage instance = null;
    Storage(){
        
    }
   
    
   public static Storage getInstance(){
       if (instance == null){
           instance = new Storage();
       }
       return instance;
   }
   
   private void addPackage(Package pack){
       Storage inst = getInstance();
       inst.packages.add(pack);
       
   }
   public ArrayList getList(){
       return packages;
   }
   
}
