/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harkkatyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Mikael
 */
public class FXMLLoggerController implements Initializable {
    @FXML
    private TextArea logTextArea;
    @FXML
    private Label createdCounter;
    @FXML
    private Label sentCounter;
    @FXML
    private Button closeButton;

    LogKeeper log = LogKeeper.getInstance();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        logTextArea.appendText(log.getText());
        createdCounter.setText(String.valueOf(log.getPackCounter()));
        sentCounter.setText(String.valueOf(log.getSentCounter()));
    }    

    @FXML
    private void closeButtonAction(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
}
