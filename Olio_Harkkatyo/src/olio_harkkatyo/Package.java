
package olio_harkkatyo;

/** Mikael Sommarberg - 0420191
 */
public abstract class Package {
    protected int maxDist;
    protected int chanceToBreak;
    protected int maxSize;
    protected int deliveryTime;
    protected int maxWeight;
    protected double fromLat,fromLng,toLat,toLng;
    public Object object;
    
    public Package(int a,int b,int c, int d,int e){
        maxDist = a;
        chanceToBreak = b;
        maxSize = c;
        deliveryTime = d;
        maxWeight = e;
    }
    Object getObject(){
        return object;
    }
    
    double getFromLat(){
        return fromLat;
    }
    
    double getToLat(){
        return toLat;
    }
    double getFromLng(){
        return fromLng;
    }
    double getToLng(){
        return toLng;
    }
    int getDeliveryTime(){
        return deliveryTime;
    }
    int getChanceToBreak(){
        return chanceToBreak;
    }
}

class FirstClass extends Package{
        public FirstClass(Object obj,double a,double b,double c, double d){
            super(150,100,99999,1,10000);
            object = obj;
            fromLat=a;
            fromLng=b;
            toLat=c;
            toLng=d;
        }
    }

class SecondClass extends Package{
        public SecondClass(Object custom,double a,double b,double c, double d){
            super(99999,0,50,2,50000);
            object = custom;
            fromLat=a;
            fromLng=b;
            toLat=c;
            toLng=d;
        }
    }

class ThirdClass extends Package{
        public ThirdClass(Object custom,double a,double b,double c, double d){
            super(99999,50,99999,3,99999);
            object = custom;
            fromLat=a;
            fromLng=b;
            toLat=c;
            toLng=d;
        }
    }