
package olio_harkkatyo;

/** Mikael Sommarberg - 0420191
 */
public class LogKeeper {
    public String data; //Public so there is no issues between classes writing here
    static LogKeeper instance;
    static int packCounter = 0;
    static int sentCounter = 0;
    LogKeeper(){
        data ="";
    }
    
    static LogKeeper getInstance(){
        if(instance == null){
            instance = new LogKeeper();
        }
        return instance;
    }
    void appendLog(String log){
        data += (log +"\n");
    }
    String getText(){
        return data;
    }
    String getCounters(){
        return ("Paketteja luotiin: "+String.valueOf(packCounter)+" ja lähetettiin: "+String.valueOf(sentCounter));
    }
    int getPackCounter(){
        return packCounter;
    }
    
    int getSentCounter(){
        return sentCounter;
    }
    
    static void addPackCounter(){
       packCounter += 1; 
    }
    static void addSentCounter(){
       sentCounter += 1; 
    }
    String getStorage(){
        String str="";
        Package pck;
        Storage storage = Storage.getInstance();
        while (!storage.getList().isEmpty()){
            pck=(Package) storage.getList().get(0);
            str+="Varastoon jäi paketti, joka sisälsi"+pck.getObject().getName()+"\n";
            storage.getList().remove(0);
        }
        return str;
    }
}
    