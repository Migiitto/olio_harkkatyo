
package olio_harkkatyo;

/** Mikael Sommarberg - 0420191
 */
public class Object {
    private int size;
    private int weight;
    private int breakable;
    private String name;
    Object(int a, int b, int c,String d){
        size = a;
        weight = b;
        breakable = c;
        name = d;
    }
    int getBreakable(){
        return breakable;
    }
    
    int getWeight(){
        return weight;
    }
    String getName(){
        return name;
    }
    int getSize(){
        return size;
    }
}

class Object1 extends Object{       //MLP-Figure
    public Object1(){
        super(50,500,0,"MLP-figure");
    
    }
}
class Object2 extends Object{       //GayStation4
    public Object2(){
        super(150,10000,1,"GayStation4 -konsoli");
    
    }
}

class Object3 extends Object{       //100 Cheek DVD
    public Object3(){
        super(500,2000,0,"100 Cheek DVD -levyä");
    
    }
}

class Object4 extends Object{       //Misaki-pillow
    public Object4(){
        super(1000,1000,0,"Misaki-tyyny");
    
    }
}

class Object5 extends Object{       //Tsimms Uber PC
    public Object5(){
        super(1000,20000,1,"Tsimms Uber PC");
    
    }
}

class Object6 extends Object{       //Deekkars Dishwasher
    public Object6(){
        super(2000,50000,0,"Teekkarin astianpesukone");
    
    }
}