/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harkkatyo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Mikael
 */
public class Olio_Harkkatyo extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        File file = new File("Kuitti.txt");
        final FileWriter fileWriter = new FileWriter(file);
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        //Hook for writing log on shutdown event
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable(){
            @Override
            public void run() {
                    LogKeeper log = LogKeeper.getInstance();
                try {
                    fileWriter.write(log.getText());
                    fileWriter.write(log.getStorage());
                    fileWriter.write("--------------------\n");
                    fileWriter.write(log.getCounters());
                    fileWriter.close();
                } catch (IOException ex) {
                    Logger.getLogger(Olio_Harkkatyo.class.getName()).log(Level.SEVERE, null, ex);
                }
                    System.out.println("Log wrote successfully!");
                } 
            }));
    }
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        launch(args);
    }
    
}
