
package olio_harkkatyo;

/** Mikael Sommarberg - 0420191
 */
public class GeoPoint {
    double lat,lng;
    GeoPoint(double lat, double lng){
        this.lat = lat;
        this.lng = lng;
    }
}
