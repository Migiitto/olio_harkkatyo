/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harkkatyo;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import netscape.javascript.JSObject;

/**
 * FXML Controller class
 *
 * @author Mikael
 */
public class FXMLPackageController implements Initializable {
    @FXML
    private ComboBox<String> objectPicker;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField SizeTextField;
    @FXML
    private TextField weightTextField;
    @FXML
    private CheckBox breakableCheckBox;
    @FXML
    private ComboBox<String> fromComboBox;
    @FXML
    private ComboBox<String> toComboBox;
    @FXML
    private ToggleGroup postageGroup;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button cancelButton;
    @FXML
    private RadioButton firstClass;
    @FXML
    private RadioButton secondClass;
    @FXML
    private RadioButton thirdClass;
    @FXML
    private WebView hiddenWebView;
    @FXML
    private Label errorLabel;
    
    DataBuilder data=DataBuilder.getInstance();
    SmartPost sp;
    Storage storage=Storage.getInstance();
    

    LogKeeper log = LogKeeper.getInstance();
    
    
    public void initialize(URL url, ResourceBundle rb) {
        
        objectPicker.getItems().addAll("Luo uusi","MLP-Figuuri","GayStation 4","100kpl Cheek DVD",
                "Misaki-tyyny","Tsimms Über PC","Teekkarin astianpesukone");
        objectPicker.setValue("Luo uusi");
        for (int i = 0; i <data.getList().size();i++){    //Adding city and address to combobox
            sp = (SmartPost) data.getList().get(i);
            fromComboBox.getItems().add((sp.city +" "+sp.address));
            toComboBox.getItems().add((sp.city +" "+sp.address));
        }
        //Creating scriptengine for distance function
        WebEngine webEngine = hiddenWebView.getEngine();
        webEngine.load(getClass().getResource("index.html").toExternalForm());
        //Setting default values
        fromComboBox.setValue("ESPOO Kutojantie 1");
        toComboBox.setValue("ROVANIEMI Koskikatu 27");
        postageGroup.selectToggle(firstClass);
        
        
    }    

    @FXML
    private void createPackageAction(ActionEvent event) {
        //Init Script for Size textfield
        ScriptEngineManager manager=new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        double value=0;
        //Create object from custom info
        Object object = null;
        int breakable;
        if (objectPicker.getValue().equals("Luo uusi")){
            try {
                java.lang.Object result = engine.eval(SizeTextField.getText());
                value = parseDouble(result.toString());
                System.out.println(value);
            } catch (ScriptException ex) {
                Logger.getLogger(FXMLPackageController.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (breakableCheckBox.isSelected()){
                breakable = 1;
            }
            else{
                breakable = 0;
            }
            object = new Object((int)value,
                    parseInt(weightTextField.getText()),
                    breakable,
                    nameTextField.getText());
        }
        //Create object from premade values
        else {
            if(objectPicker.getValue().equals("MLP-Figuuri")){
                object = new Object1();
            }
            if(objectPicker.getValue().equals("GayStation 4")){
                object = new Object2();
            }
            if(objectPicker.getValue().equals("100kpl Cheek DVD")){
                object = new Object3();
            }
            if(objectPicker.getValue().equals("Misaki-tyyny")){
                object = new Object4();
            }
            if(objectPicker.getValue().equals("Tsimms Über PC")){
                object = new Object5();
            }
            if(objectPicker.getValue().equals("Teekkarin astianpesukone")){
                object = new Object6();
            }
        }
        
        
        //Get SmartPost objects corresponding to selected ones
        SmartPost from=null,to=null;
        for (int i = 0; i <data.getList().size();i++){
            sp = (SmartPost) data.getList().get(i);
            if(fromComboBox.getValue().equals((sp.city +" "+sp.address))){
                from = sp;
            }
            if(toComboBox.getValue().equals((sp.city +" "+sp.address))){
                to = sp;
            }
        }
        //Calculate the distance of delivery
        double distance = parseDouble(hiddenWebView.getEngine().executeScript(
                "document.createPath(["+from.getLat()+","+from.getLng()+","+
                        to.getLat()+","+to.getLng()+"],'BLUE',1)").toString());
        System.out.println(distance);

        //Get selected package class and create Package-object
        RadioButton tmp =(RadioButton)postageGroup.getSelectedToggle();
        Stage stage = (Stage) createPackageButton.getScene().getWindow();
        //User error checking for FirstClass
        if (tmp.getText().equals("1. Luokka")){
            if(distance > 150.0){
                errorLabel.setText("Kuljetusmatka liian pitkä!");
            }
            if(object.getWeight() > 10000){
                errorLabel.setText("Tavara on liian painava!");
            }
            else if(distance<=150){
                storage.getList().add(new FirstClass(object,from.getLat(),from.getLng(),to.getLat(),to.getLng()));
                System.out.println("Package created successfully!");
                stage.close();
                log.appendLog("1. Luokan paketti ("+object.getName()+") "+fromComboBox.getValue()+":sta paikkaan "
                        +toComboBox.getValue()+" (pituus "+ distance+") luotu varastoon.");
                LogKeeper.addPackCounter();
                
            }
        }
        //User error checking for SecondClass
        if (tmp.getText().equals("2. Luokka")){
            if(object.getSize() > 10000){
                errorLabel.setText("Tavara on liian iso!");
            }
            if(object.getWeight() > 50000){
                errorLabel.setText("Tavara on liian painava!");
            }
            else{
                storage.getList().add(new SecondClass(object,from.getLat(),from.getLng(),to.getLat(),to.getLng()));
                System.out.println("Package created successfully!");
                log.appendLog("2. Luokan paketti ("+object.getName()+") "+fromComboBox.getValue()+":sta paikkaan "
                        +toComboBox.getValue()+" (pituus "+ distance+") luotu varastoon.");
                LogKeeper.addPackCounter();
                stage.close();
                
            }
        }
        //User error checking for ThirdClass
        
        if (tmp.getText().equals("3. Luokka")){
            if(object.getWeight()> 99999){
                errorLabel.setText("Tavara on liian painava!");
            }
            storage.getList().add(new ThirdClass(object,from.getLat(),from.getLng(),to.getLat(),to.getLng()));
            System.out.println("Package created successfully!");
            log.appendLog("3. Luokan paketti ("+object.getName()+") "+fromComboBox.getValue()+":sta paikkaan "
                        +toComboBox.getValue()+" (pituus "+ distance+") luotu varastoon.");
            LogKeeper.addPackCounter();
            stage.close();
        }
        
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
    
}
