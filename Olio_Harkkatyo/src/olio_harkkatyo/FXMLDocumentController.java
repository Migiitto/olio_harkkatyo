/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harkkatyo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Double.parseDouble;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Mikael
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private WebView mapView;
    @FXML
    private ComboBox<String> SmartPostComboBox;
    @FXML
    private Button addSmartPost;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private ComboBox<Package> packagesComboBox;
    @FXML
    private Button removePathButton;
    @FXML
    private TextArea infoBox;
    @FXML
    private Button showLogButton;
    @FXML
    private Button shutdownButton;
    @FXML
    private Button refreshPackagesButton;
    
    //Initializing some variables which are used in many places
    WebEngine webEngine;
    DataBuilder db = DataBuilder.getInstance();
    SmartPost sp;
    Storage storage = Storage.getInstance();
    Random randomGenerator = new Random();
    LogKeeper log = LogKeeper.getInstance();

    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Get data from internet and parse it to an arraylist
        for (int i = 0; i <db.getList().size();i++){    //Adding city and address to combobox
            sp = (SmartPost) db.getList().get(i);
            SmartPostComboBox.getItems().add((sp.city +" "+sp.address));
        }
        
        //Init webview and load index.html
        webEngine = mapView.getEngine();     
        webEngine.load(getClass().getResource("index.html").toExternalForm());
        
    }    

    @FXML
    private void addSmartPostAction(ActionEvent event) {
        //Init variables for adding SmartPost places on map
        String search="",info="",color="",valinta;
        valinta = SmartPostComboBox.getValue();
        for (int i = 0; i <db.getList().size();i++){
            sp = (SmartPost) db.getList().get(i);
            if(valinta.equals((sp.city +" "+sp.address))){
                search = sp.address +", "+sp.code+" "+sp.city;
                System.out.println(search); 
                info = sp.postoffice +" "+ sp.openhours;
                System.out.println(info);
                color = "red";
            }
        }
        //Magic
        webEngine.executeScript("document.goToLocation('"+search+"','"+info+"','"+color+"')");
    }

    @FXML
    private void createPackageAction(ActionEvent event) {
        //Open a new window for creating packages
        try {
            Stage packageview = new Stage();
            Parent pack = FXMLLoader.load(getClass().getResource("FXMLPackage.fxml"));
            Scene scene = new Scene(pack);
            packageview.setScene(scene);
            packageview.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void sendPackageAction(ActionEvent event) {
        Package send;
        String str;
        //User error checking
        if (packagesComboBox.getValue()==null){}
        else{
        send = packagesComboBox.getValue();
        //Get distance for later use and execute function to draw path
        Double distance = parseDouble(webEngine.executeScript("document.createPath(["+send.getFromLat()+","+send.getFromLng()+
                ","+send.getToLat()+","+send.getToLng()+"],'BLUE',"+send.getDeliveryTime()+")").toString());
        storage.getList().remove(send); //remove package from list after sending it
        packagesComboBox.getItems().clear();
        
        System.out.println("Package sent successfully!");
        log.appendLog("Paketti, joka sisälsi "+send.getObject().getName()+" on lähetetty onnistuneesti.");
        LogKeeper.addSentCounter();
        
        //Functions to print if package survived TIMO
        //FirstClass & breakable
        if((send.getObject().getBreakable() == 1) && (send.getDeliveryTime()==1)){
            
            str = "Valitettavasti TIMO rikkoi pakettisi, joka sisälsi tavaran: "+send.getObject().getName();
            infoBox.appendText(str + "\n");
            log.appendLog(str);
        }
        //FirstClass & non-breakable
        if((send.getObject().getBreakable() == 0) && (send.getDeliveryTime()==1)){
            str = "TIMO toimitti paketin, joka sisälsi: "+send.getObject().getName();
            infoBox.appendText(str + "\n");
            log.appendLog(str);
        }
        //SecondClass
        if((send.getDeliveryTime()==2)){
            str = "TIMO toimitti paketin, joka sisälsi: "+send.getObject().getName();
            infoBox.appendText(str+"\n");
            log.appendLog(str);
        }
        //ThirdClass & non-breakable
        if((send.getObject().getBreakable()==0) && (send.getDeliveryTime()==3)){
            str="TIMO toimitti paketin, joka sisälsi: "+send.getObject().getName();
            infoBox.appendText(str+"\n");
            log.appendLog(str);
        }
        //ThirdClass & breakable
        if((send.getObject().getBreakable() == 1) && (send.getDeliveryTime()==3)){
            //Some math for giving TIMO a chance to break object
            double value = (send.chanceToBreak/(send.maxWeight/send.object.getWeight()));
            System.out.println(value);
            int random = randomGenerator.nextInt(100);
            System.out.print(random/2);
            //Random value determines if object is broken.
            if((random/2) > value){
                str = "Valitettavasti TIMO rikkoi pakettisi, joka sisälsi tavaran: "+send.getObject().getName();
                infoBox.appendText(str+"\n");
                log.appendLog(str);
            }
            else{
                str = "TIMO toimitti paketin, joka sisälsi: "+send.getObject().getName();
                infoBox.appendText(str+"\n");
                log.appendLog(str);
            }
        }
        }
        
    }

    @FXML
    private void removePathAction(ActionEvent event) {
        //Clear map and also clears the infobox below map
        webEngine.executeScript("document.deletePaths()");
        infoBox.clear();
        
    }


    @FXML
    private void showLogAction(ActionEvent event) {
        //Open a new window for creating packages
        try {
            Stage packageview = new Stage();
            Parent pack = FXMLLoader.load(getClass().getResource("FXMLLogger.fxml"));
            Scene scene = new Scene(pack);
            packageview.setScene(scene);
            packageview.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void shutdownAction(ActionEvent event) {
        System.exit(1);
    }

    @FXML
    private void refreshPackagesAction(ActionEvent event) {
        //Opening menu causes the package list to refresh
        packagesComboBox.getItems().clear();
        for (int i = 0; i <storage.getList().size();i++){
            if(packagesComboBox.getItems().contains((Package)storage.getList().get(i))){
                
            }
            else{
                packagesComboBox.getItems().add((Package)storage.getList().get(i));
            }
        }
    }
    
}
