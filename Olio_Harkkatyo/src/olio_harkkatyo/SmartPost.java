
package olio_harkkatyo;

/** Mikael Sommarberg - 0420191
 */
public class SmartPost {
    String code;
    String city,address,postoffice,openhours;
    GeoPoint geopoint;
    
    SmartPost(String i, String a, String b, String c,String j,double d,double e){
        code = i;
        city = a;
        address=b;
        openhours = j;
        postoffice=c;
        geopoint = new GeoPoint(d,e);
        //System.out.println(c);
        
    }
    
    public String getAddress(){
        return address;
    }
    
    public double getLng(){
        return geopoint.lng;
    }
    public double getLat(){
        return geopoint.lat;
    }
}
