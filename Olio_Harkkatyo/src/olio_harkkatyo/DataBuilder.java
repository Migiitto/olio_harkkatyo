
package olio_harkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import static java.lang.Integer.parseInt;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import static java.lang.Double.parseDouble;

/** Mikael Sommarberg - 0420191
 */
public class DataBuilder {
    private Document doc;
    private ArrayList<SmartPost> smartPosts = new ArrayList();
    private String data;
    static DataBuilder instance = null;
    
    DataBuilder(){
        data = getData();
        doc = smartpostXML(data);
        parseData(doc, smartPosts);
    }
    
    public static DataBuilder getInstance(){
        if (instance == null){
            instance = new DataBuilder();
        }
        return instance;
    }
    
    
    
    ArrayList getList(){
        return smartPosts;
    }
    
    
    
    private void parseData(Document doc, ArrayList smartPosts){
        NodeList nodes = doc.getElementsByTagName("place");
        System.out.println("Parsing data");
        for(int i=0; i<nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            smartPosts.add(new SmartPost(e.getElementsByTagName("code").item(0).getTextContent(),
                    e.getElementsByTagName("city").item(0).getTextContent(),
                    e.getElementsByTagName("address").item(0).getTextContent(),
                    e.getElementsByTagName("postoffice").item(0).getTextContent(),
                    e.getElementsByTagName("availability").item(0).getTextContent(),
                    parseDouble(e.getElementsByTagName("lat").item(0).getTextContent()),
                    parseDouble(e.getElementsByTagName("lng").item(0).getTextContent())));
        }
        System.out.print("Parsed ");
        System.out.print(smartPosts.size());
        System.out.println(" smartpost places");
        
    }
    
    
    private Document smartpostXML(String data){
        System.out.println("Building document");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(data)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Document built!");
        return doc;
    }
    
    
    private String getData() {
        String line,data="";
        System.out.println("Getting data");
        try {
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            line = in.readLine();
            while (line !=null){
                data += line +"\n";
                line=in.readLine();
            }
            System.out.println("Got data");
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return data;
    }
    
}   
